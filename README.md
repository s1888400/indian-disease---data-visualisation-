Data Infomation:

Title: A Medical History of British India
Description: This dataset contains 1 plain text readme file; 1 inventory CSV file; 117,022 ALTOXML files; 468 METS files; 120,903 image files.
Owner: National Library of Scotland
Creator: National Library of Scotland
Date created: 27/08/2019
Rights: Item-level rights information can be found in the METS files. Items in this dataset are free of known copyright and in the public domain.
Contact: digital.scholarship@nls.uk


Data Analysis:

I choose a book "74461102-Cholera in India, 1862 to 1881" to analysis.

As the data has three forms: images, xmls and txt. I read some of the Images to make sure the content and recover the districts of a clear sheet at the beginning of the book to see whether it can show something clear. - Tool to analysis: glob, elementree.

(1) Create a chart that show the times of the districts showed in the book, and see that the times is totally different whcih means many data of the distrcts may lost or did not be collected.

(2) Create a wordcloud to analysis the word frquency. It can be seen that the "Bengal province" has the most times just behind the "cholera", after that is the "district".

(3)After analysing the whole book, I can see the whole book described the diseases happened in Bengal province. And I found the table in the end of the book is the summary of the overall data, and the last part is the data about the European troops, Native troops and prisoners in jails which attracted me.  Then I use xml position to recover the sheet about this part and see some rend of these three groups infection and caculate the death rate.

<<<<<<< HEAD
Data Visualisation:

https://uoe-my.sharepoint.com/:v:/g/personal/s1888400_ed_ac_uk/EaR6qO_YQUBGmxuS3GiigwcB9PIe5VD9yPMd1Fg2RToLeQ?e=Fyf1Za
=======
Conclusion:

1.	Cholera infected most people in Jail, while European troops have the least infected population.
2.	Calcutta is the capital of British India, located in the Ganges delta, which is the birthplace of the Cholera. That is the reason why the area with the highest concentration of cholera infections is Calcutta.
3.	Jail and Native troops are distributed more in the same region than European troops.
4.	All of the infected people are distributed in British India.
5.	All of the Jail was located in British India.
6.	1866 was an outbreak of Cholera during these five years based on data.
7.	No one in eastern and western Bengal has contracted Cholera in these five years. As data are not provided and indigenous peoples from some parts of India are not shown on the map, we can deduce whether they have not recorded the data or no one was actually infected with Cholera.
8.	Comparison of mortality lines: The European army has always been the lowest, and Jail has ever had the highest mortality, but all three have shown a downward trend. This can deduce the different levels of medical care these three groups received at the time, and the mortality rate of the European troops is very low, maybe because the medical conditions of the European army are more advanced than others.
>>>>>>> 927cb4ecfef37a52c9cdb9852c80a706a9c24776
